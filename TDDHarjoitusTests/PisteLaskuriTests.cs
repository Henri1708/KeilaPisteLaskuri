﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KeilaPisteLaskuriOhjelma;
using KeilaPisteLaskuriTests;

namespace KeilaPisteLaskuriTests
{
    [TestFixture]
    public class KeilaPisteLaskuriTest
    {

        public KeilaPisteLaskuri laskin = null;

        [SetUp]
        public void TestienAlustus()
        {
            this.laskin = new KeilaPisteLaskuri();    //Näin ei tarvitse joka testiin erikseen kirjoittaa: laskin = new KeilaPisteLaskuri();
        }

        [Test]
        public void YksikäänEiKaadu()
        {
            ToistaHeitto(0, 20);
            int tulos = laskin.GetTulos();
            Assert.AreEqual(0, tulos);
        }

        [Test]
        public void JokaHeitollaYksiKaatuu()
        {
            ToistaHeitto(1, 20);
            int tulos = laskin.GetTulos();
            Assert.AreEqual(20, tulos);
        }

        [Test]
        public void Paikko()
        {
            Heitä(6, 4, 9, 0);
            ToistaHeitto(0, 16);
            int tulos = laskin.GetTulos();
            Assert.AreEqual(6 + 4 + 9 * 2, tulos);
        }

        [Test]
        public void Kaato()
        {
            Heitä(10, 6, 2);
            ToistaHeitto(0, 16);
            int tulos = laskin.GetTulos();
            Assert.AreEqual(10 + 6 * 2 + 2 * 2, tulos);
        }

        [Test]
        public void TäydetPisteet()
        {
            ToistaHeitto(10, 12);
            int tulos = laskin.GetTulos();
            Assert.AreEqual(300, tulos);
        }

        private void Heitä(params int[] keilat)
        {
            foreach (int keila in keilat)
            {
                laskin.Heitä(keila);
            }
        }

        private void ToistaHeitto(int keilat, int määrä)
        {
            for (int i = 0; i < määrä; i++)
            {
                laskin.Heitä(keilat);
            }
        }                   
    }
}
   