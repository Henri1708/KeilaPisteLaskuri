﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KeilaPisteLaskuriOhjelma
{
    public class KeilaPisteLaskuri
    {
        int[] heitot = new int[21];
        int seuraavaHeitto = 0;

        public void Heitä(int keilat)
        {
            heitot[seuraavaHeitto++] = keilat;
        }

        public int GetTulos()
        {
            int heittoMäärä = 0;
            int tulos = 0;

            for (int laskin = 0; laskin < 10; laskin++)
            {
                if (heitot[heittoMäärä] == 10)
                {
                    //kaato
                    tulos += 10 + heitot[heittoMäärä + 1] + heitot[heittoMäärä + 2];
                    heittoMäärä++;
                }
                else if (heitot[heittoMäärä] + heitot[heittoMäärä + 1] ==10)
                {
                    //paikko
                    tulos += 10 + heitot[heittoMäärä + 2];
                    heittoMäärä += 2;
                }
                else
                {
                    tulos += heitot[heittoMäärä] + heitot[heittoMäärä + 1];
                    heittoMäärä += 2;
                }
            }
            return tulos;
        }
    }
    }
